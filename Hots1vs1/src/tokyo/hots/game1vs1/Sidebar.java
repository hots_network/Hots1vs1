package tokyo.hots.game1vs1;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import tokyo.hots.game1vs1.session.SessionPlayer;

public class Sidebar {

	Hots1vs1Plugin plugin;

	public Sidebar(Hots1vs1Plugin plugin) {
		this.plugin = plugin;
	}

	//  1vs1  //
	//
	// Players: 10
	//
	// In Lobby: 4
	// In Game: 6
	//
	// Win: 10
	// Lose: 10
	//
	// Hots Network

	@SuppressWarnings("deprecation")
	public void update(){
		for(Player player : Bukkit.getOnlinePlayers()){
			SessionPlayer splayer = plugin.getSessionPlayerManager().getSessionPlayer(player);
			if(!splayer.isInMatch()){
				ScoreboardManager manager = Bukkit.getScoreboardManager();
				Scoreboard board = manager.getNewScoreboard();
				Objective obj = board.registerNewObjective("abc", "dummy");

				obj.setDisplayName("" + ChatColor.AQUA + ChatColor.BOLD +  "Hots 1vs1");
				obj.setDisplaySlot(DisplaySlot.SIDEBAR);

				Score a = obj.getScore(" ");
				a.setScore(-1);

				Score b = obj.getScore("Players: " + ChatColor.GOLD + Bukkit.getOnlinePlayers().length);
				b.setScore(-2);

				Score c = obj.getScore("  ");
				c.setScore(-3);

				Score d = obj.getScore("In Lobby: " + ChatColor.GOLD + plugin.getSessionManager().getInLobby());
				d.setScore(-4);

				Score e = obj.getScore("In Game: " + ChatColor.GOLD + plugin.getSessionManager().getInGame());
				e.setScore(-5);

				Score f = obj.getScore("   ");
				f.setScore(-6);

				Score g = obj.getScore("Won: " + ChatColor.GOLD + splayer.getPlayerData().getWon());
				g.setScore(-7);

				Score h = obj.getScore("Lost: "+ ChatColor.GOLD + splayer.getPlayerData().getLost());
				h.setScore(-8);

				Score i = obj.getScore("    ");
				i.setScore(-9);

				Score j = obj.getScore(ChatColor.YELLOW + "Hots Network");
				j.setScore(-10);
				player.setScoreboard(board);
			}
		}
	}
}
