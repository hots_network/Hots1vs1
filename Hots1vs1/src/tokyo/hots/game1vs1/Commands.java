package tokyo.hots.game1vs1;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tokyo.hots.game1vs1.session.SessionPlayer;
import tokyo.hots.game1vs1.util.ChatMessage;

public class Commands implements CommandExecutor{

	Hots1vs1Plugin plugin;

	public Commands(Hots1vs1Plugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		if(args.length == 0){
			sendHelp(sender);
		}
		if(args.length == 1 && args[0].equals("menu")){
			SessionPlayer s = plugin.getSessionPlayerManager().getSessionPlayer(player);
			s.openMenu(plugin.getMenuManager().getSessionMenu());
		}
		if(args.length == 1 && args[0].equals("setlobby")){
			plugin.setLobby(player);
		}
		if(args.length == 2 && args[0].equals("create")){
			plugin.getMapManager().create(sender, args[1]);
		}
		if(args.length == 3 && args[0].equals("setspawn")){
			plugin.getMapManager().setSpawn((Player)sender, Integer.parseInt(args[1]), args[2]);
		}
		if(args.length == 2 && args[0].equals("savekit")){
			plugin.getKitManager().save(player, args[1]);
		}
		//1vs1 setkitedit <name>
		if(args.length == 2 && args[0].equals("setkitedit")){
			plugin.getLocations().saveLocation(player, args[1]);
		}
		return false;
	}

	public void sendHelp(CommandSender sender){
		ChatMessage.send(sender, "/1vs1 setlobby");
		ChatMessage.send(sender, "/1vs1 create <map_name>");
		ChatMessage.send(sender, "/1vs1 setspawn 1 <map_name>");
		ChatMessage.send(sender, "/1vs1 setspawn 2 <map_name>");
		ChatMessage.send(sender, "/1vs1 savekit <sg, gapplesg, pot, builduhc>");
		ChatMessage.send(sender, "/1vs1 setkitedit <sg, gapplesg, pot, builduhc>");
	}
}
