package tokyo.hots.game1vs1;

import java.io.File;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import tokyo.hots.game1vs1.util.ChatMessage;
import tokyo.hots.game1vs1.util.YamlEditor;

public class Locations {

	private Location sg_editkit, gapplesg_editkit, pot_editkit, builduhc_editkit;

	Hots1vs1Plugin plugin;

	public Locations(Hots1vs1Plugin plugin) {
		sg_editkit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "SG_EditKit.yml")).getLocation("World", "X", "Y", "Z", "Yaw", "Pitch");
		gapplesg_editkit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "GappleSG_EditKit.yml")).getLocation("World", "X", "Y", "Z", "Yaw", "Pitch");
		pot_editkit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "Pot_EditKit.yml")).getLocation("World", "X", "Y", "Z", "Yaw", "Pitch");
		builduhc_editkit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "BuildUHC_EditKit.yml")).getLocation("World", "X", "Y", "Z", "Yaw", "Pitch");
	}

	public Location getSg_editkit() {
		return sg_editkit;
	}

	public Location getGapplesg_editkit() {
		return gapplesg_editkit;
	}

	public Location getPot_editkit() {
		return pot_editkit;
	}

	public Location getBuilduhc_editkit() {
		return builduhc_editkit;
	}

	public void setBuilduhc_editkit(Location builduhc_editkit) {
		this.builduhc_editkit = builduhc_editkit;
		YamlEditor edit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "BuildUHC_EditKit.yml"));
		edit.setValue("World", builduhc_editkit.getWorld().getName());
		edit.setValue("X", builduhc_editkit.getX());
		edit.setValue("Y", builduhc_editkit.getY());
		edit.setValue("Z", builduhc_editkit.getZ());
		edit.setValue("Yaw", builduhc_editkit.getYaw());
		edit.setValue("Pitch", builduhc_editkit.getPitch());
		edit.save();
	}

	public void setGapplesg_editkit(Location gapplesg_editkit) {
		this.gapplesg_editkit = gapplesg_editkit;
		YamlEditor edit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "GappleSG_EditKit.yml"));
		edit.setValue("World", gapplesg_editkit.getWorld().getName());
		edit.setValue("X", builduhc_editkit.getX());
		edit.setValue("Y", builduhc_editkit.getY());
		edit.setValue("Z", builduhc_editkit.getZ());
		edit.setValue("Yaw", builduhc_editkit.getYaw());
		edit.setValue("Pitch", builduhc_editkit.getPitch());
		edit.save();
	}

	public void setPot_editkit(Location pot_editkit) {
		this.pot_editkit = pot_editkit;
		YamlEditor edit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "Pot_EditKit.yml"));
		edit.setValue("World", pot_editkit.getWorld().getName());
		edit.setValue("X", builduhc_editkit.getX());
		edit.setValue("Y", builduhc_editkit.getY());
		edit.setValue("Z", builduhc_editkit.getZ());
		edit.setValue("Yaw", builduhc_editkit.getYaw());
		edit.setValue("Pitch", builduhc_editkit.getPitch());
		edit.save();
	}

	public void setSg_editkit(Location sg_editkit) {
		this.sg_editkit = sg_editkit;
		YamlEditor edit = new YamlEditor(new File(Hots1vs1Plugin.getPluginFolder() + "/locations/" + "SG_EditKit.yml"));
		edit.setValue("World", sg_editkit.getWorld().getName());
		edit.setValue("X", builduhc_editkit.getX());
		edit.setValue("Y", builduhc_editkit.getY());
		edit.setValue("Z", builduhc_editkit.getZ());
		edit.setValue("Yaw", builduhc_editkit.getYaw());
		edit.setValue("Pitch", builduhc_editkit.getPitch());
		edit.save();
	}

	public void saveLocation(Player player, String name){
		Location location = player.getLocation();
		if(name.equals("sg")){
			setSg_editkit(location);
		}
		if(name.equals("gapplesg")){
			setGapplesg_editkit(location);
		}
		if(name.equals("builduhc")){
			setBuilduhc_editkit(location);
		}
		if(name.equals("pot")){
			setPot_editkit(location);
		}
		ChatMessage.send(player, "Saved kit edit of the location");
	}
}
