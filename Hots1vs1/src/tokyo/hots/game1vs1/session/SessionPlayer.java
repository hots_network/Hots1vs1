package tokyo.hots.game1vs1.session;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Scoreboard;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.data.PlayerData;
import tokyo.hots.game1vs1.menu.Menu;
import tokyo.hots.game1vs1.session.kit.Kit;
import tokyo.hots.game1vs1.util.ChatMessage;
import tokyo.hots.game1vs1.util.ItemUtil;

public class SessionPlayer {

	Hots1vs1Plugin plugin;

	//Bukkit player
	private Player player;

	//予約しているか
	private boolean queue;

	//試合に参加しているか
	private boolean inMatch;

	//参加中のセッション
	private Session session;

	private PlayerData playerData;

	public SessionPlayer(Player player, Hots1vs1Plugin plguin) {
		this.plugin = plguin;
		this.player = player;
		this.queue = false;
		this.session = null;
		this.inMatch = false;
		this.playerData = null;
	}

	public int getHealth(){
		Damageable d = (Damageable) player;
		int health = (int) d.getHealth();
		return health;
	}

	public int getFoodLevel(){
		return player.getFoodLevel();
	}

	public List<ItemStack> getInventoryItems(){
		List<ItemStack> items = Lists.newArrayList();
		for(int i = 0; i < getInventory().getSize(); i++){
			items.add(getInventory().getItem(i));
		}
		return items;
	}

	public void setPlayerData(PlayerData playerData) {
		this.playerData = playerData;
	}

	public PlayerData getPlayerData() {
		return playerData;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean isQueue() {
		return queue;
	}

	public void sendMessage(String message){
		ChatMessage.send(player, message);
	}

	public String getName(){
		return player.getName();
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public void openMenu(Menu menu) {
		player.openInventory(menu.getInventory());
	}

	public void setQueue(boolean queue) {
		this.queue = queue;
	}

	public void leaveSession(){
		if(queue){
			session.getMap().setUsing(false);
			setQueue(false);
			plugin.getSessionManager().getSessions().remove(session);
			setSession(null);
			if(player != null){
				sendMessage("Successfully. Leave session");
			}
			return;
		}
		if(player != null){
			sendMessage("You haven't queued");
		}
	}

	public boolean isInMatch() {
		return inMatch;
	}

	public void setInMatch(boolean inMatch) {
		this.inMatch = inMatch;
	}

	public void teleport(Location location){
		player.teleport(location);
	}

	public void playSound(Location arg0, Sound arg1, float arg2, float arg3){
		player.playSound(arg0, arg1, arg2, arg3);
	}

	public Location getLocation(){
		return player.getLocation();
	}

	public void setSidebar(Scoreboard board){
		player.setScoreboard(board);
	}

	public Inventory getInventory(){
		return player.getInventory();
	}

	public void setDefaultItems(){
		getInventory().clear();
		getInventory().setItem(0, ItemUtil.getItemStack("" + ChatColor.AQUA + ChatColor.BOLD + "Match Sessions", Material.DIAMOND_SWORD, ChatColor.GRAY + "Click to open the 1vs1 menu"));
		getInventory().setItem(1, ItemUtil.getItemStack("" + ChatColor.GREEN + ChatColor.BOLD + "My Profile", Material.WATCH, ChatColor.GRAY + "Click to open your profile menu"));
		getInventory().setItem(7, ItemUtil.getItemStack("" + ChatColor.GREEN + ChatColor.BOLD + "Edit Kit", Material.ANVIL, ChatColor.GRAY + "Click to open edit kit select menu"));
		getInventory().setItem(8, ItemUtil.getItemStack("" + ChatColor.YELLOW + ChatColor.BOLD + "Leader Board", Material.BOOK, ChatColor.GRAY + "Click to open the leader board"));
	}

	public void clearArmor(){
		player.getEquipment().setHelmet(null);
		player.getEquipment().setChestplate(null);
		player.getEquipment().setLeggings(null);
		player.getEquipment().setBoots(null);
	}

	public void setKit(Kit kit){
		getInventory().clear();

		for(int i = 0; i < kit.getItems().size(); i++){
			getInventory().setItem(i, kit.getItems().get(i));
		}
		List<ItemStack> armors = kit.getArmors();
		player.getEquipment().setHelmet(armors.get(3));
		player.getEquipment().setChestplate(armors.get(2));
		player.getEquipment().setLeggings(armors.get(1));
		player.getEquipment().setBoots(armors.get(0));
	}

}