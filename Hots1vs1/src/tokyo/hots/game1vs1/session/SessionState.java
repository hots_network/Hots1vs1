package tokyo.hots.game1vs1.session;

public enum SessionState {

	WAITING, TELEPORTING, PREGAME, MATCHING, ENDING
}
