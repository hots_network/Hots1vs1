package tokyo.hots.game1vs1.session;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class SessionSidebar {

	private Session session;

	public SessionSidebar(Session session) {
		this.session = session;
	}

	public void showSidebar(){
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective obj = board.registerNewObjective("session", "dummy");

		obj.setDisplayName("" + ChatColor.AQUA + ChatColor.BOLD + "1vs1");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);

		Score a = obj.getScore(" ");
		a.setScore(-1);

		Score b = obj.getScore(session.getStateText());
		b.setScore(-2);

		Score c = obj.getScore("Time: " + ChatColor.YELLOW + session.getTime());
		c.setScore(-3);

		Score d = obj.getScore("Kit: " + session.getKitTypeText());
		d.setScore(-4);

		Score e = obj.getScore("  ");
		e.setScore(-5);

		Score f = obj.getScore("Players:");
		f.setScore(-6);

		Score g = obj.getScore(checkLength(session.getPlayer1().getName()));
		g.setScore(-7);

		Score h = obj.getScore(checkLength(session.getPlayer2().getName()));
		h.setScore(-8);

		Score i = obj.getScore("   ");
		i.setScore(-9);

		Score j = obj.getScore(ChatColor.YELLOW + "Hots Network");
		j.setScore(-10);

		session.getPlayer1().setSidebar(board);
		session.getPlayer2().setSidebar(board);
	}

	public String timeFormat(int sc){
		float fun = sc / 60;
		float byou = sc % 60;
		int funn = (int)Math.floor(fun);
		int byouu = (int)byou; String funnn = String.valueOf(funn);
		String byouuu = String.valueOf(byouu);
		int funkazu = funnn.length();
		int byoukazu = byouuu.length();
		if (funkazu == 1) {
			funnn = "0" + funnn;
		}
		if (byoukazu == 1) {
			byouuu = "0" + byouuu;
		}
		String time = funnn + ":" + byouuu;
		return time;
	}

	public String checkLength(String name){
		if(name.length() > 16){
			return ChatColor.BLUE + name.substring(17, name.length());
		}
		return ChatColor.BLUE + name;
	}
}