package tokyo.hots.game1vs1.session.kit;

import java.util.List;

import org.bukkit.inventory.ItemStack;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.util.YamlEditor;

public class Kit {

	private KitType type;

	private List<ItemStack> items;

	private List<ItemStack> armors;

	public Kit(KitType type, YamlEditor edit) {
		this.type = type;
		this.items = Lists.newArrayList();
		this.items = edit.getItemStackList("Items");
		this.armors = Lists.newArrayList();
		this.armors = edit.getItemStackList("Armors");
	}

	public KitType getType() {
		return type;
	}

	public List<ItemStack> getItems() {
		return items;
	}

	public List<ItemStack> getArmors() {
		return armors;
	}


}
