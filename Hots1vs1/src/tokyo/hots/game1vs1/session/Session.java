package tokyo.hots.game1vs1.session;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.data.Map;
import tokyo.hots.game1vs1.manager.KitManager;
import tokyo.hots.game1vs1.session.kit.Kit;
import tokyo.hots.game1vs1.session.kit.KitType;
import tokyo.hots.game1vs1.util.ItemUtil;

public class Session {

	Hots1vs1Plugin plugin;

	//試合中のプレイヤー
	private SessionPlayer player1, player2;

	//マップ
	private Map map;

	//時間
	private int time;

	//状況
	private SessionState state;

	//id
	private int id;

	//kit
	private KitType kitType;

	//有効であるか
	private boolean enabled;

	private SessionSidebar sidebar;

	public Session(SessionPlayer player, Hots1vs1Plugin plugin) {
		this.plugin = plugin;
		this.player1 = player;
		this.player2 = null;
		this.map = Hots1vs1Plugin.getPlugin().getMapManager().randomMap();
		this.time = 0;
		this.state = SessionState.WAITING;
		this.id = Hots1vs1Plugin.getPlugin().getSessionManager().getSessions().size()+1;
		this.kitType = null;
		this.enabled = false;
		this.sidebar = null;
		map.setUsing(true);
	}

	public void randomKit(){
		List<KitType> types = Lists.newArrayList();
		types.add(KitType.BUILD_UHC);
		types.add(KitType.SG);
		types.add(KitType.GAPPLE_SG);
		types.add(KitType.POT);
		Collections.shuffle(types);
	}

	public SessionPlayer getPlayer1() {
		return player1;
	}

	public SessionPlayer getPlayer2() {
		return player2;
	}

	public void setPlayer2(SessionPlayer player2) {
		this.player2 = player2;
	}


	public void setState(SessionState state) {
		this.state = state;
	}

	public Map getMap() {
		return map;
	}

	public int getTime() {
		return time;
	}

	public SessionState getState() {
		return state;
	}

	public int getId() {
		return id;
	}

	public Material getMaterial(){
		if(state != SessionState.WAITING){
			return Material.REDSTONE_BLOCK;
		}
		return Material.EMERALD_BLOCK;
	}

	public String getStateText(){
		if(state == SessionState.TELEPORTING){
			return ChatColor.LIGHT_PURPLE + "Teleporting";
		}
		if(state == SessionState.MATCHING){
			return ChatColor.RED + "Matching";
		}
		if(state == SessionState.WAITING){
			return ChatColor.GREEN + "Waiting";
		}
		if(state == SessionState.ENDING){
			return ChatColor.YELLOW + "Ending";
		}
		if(state == SessionState.PREGAME){
			return ChatColor.YELLOW + "PreGame";
		}
		return null;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public KitType getKitType() {
		return kitType;
	}

	public String getKitTypeText(){
		if(kitType == KitType.SG){
			return ChatColor.YELLOW + "SG";
		}
		if(kitType == KitType.BUILD_UHC){
			return ChatColor.YELLOW + "BuildUHC";
		}
		if(kitType == KitType.POT){
			return ChatColor.YELLOW + "Pot";
		}
		if(kitType == KitType.GAPPLE_SG){
			return ChatColor.YELLOW + "GappleSG";
		}
		return null;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public Kit getKit(){
		KitManager mng = plugin.getKitManager();
		if(kitType == KitType.SG){
			return mng.getSG_Kit();
		}
		if(kitType == KitType.BUILD_UHC){
			return mng.getBuildUHC_Kit();
		}
		if(kitType == KitType.GAPPLE_SG){
			return mng.getGappleSG_Kit();
		}
		if(kitType == KitType.POT){
			return mng.getPot_Kit();
		}
		return null;
	}
	public void start() {
		player1.setInMatch(true);
		player2.setInMatch(true);
		player1.setQueue(false);
		player2.setQueue(false);

		setTime(4);
		setState(SessionState.TELEPORTING);

		player1.sendMessage(ChatColor.BLUE + player1.getName() + ChatColor.GRAY + " vs " + ChatColor.BLUE + player2.getName());
		player2.sendMessage(ChatColor.BLUE + player1.getName() + ChatColor.GRAY + " vs " + ChatColor.BLUE + player2.getName());
		player1.sendMessage("You will teleport to match arena after 3 seconds");
		player2.sendMessage("You will teleport to match arena after 3 seconds");

		sidebar = new SessionSidebar(this);

		new BukkitRunnable() {
			@Override
			public void run() {
				if(time != 0){
					time--;
					if(state == SessionState.PREGAME || state == SessionState.MATCHING || state == SessionState.ENDING){
						sidebar.showSidebar();
					}
					if(state == SessionState.PREGAME){
						player1.sendMessage("Starting in " + ChatColor.AQUA + time + ChatColor.GRAY + " seconds");
						player2.sendMessage("Starting in " + ChatColor.AQUA + time + ChatColor.GRAY + " seconds");
						player1.playSound(player1.getLocation(), Sound.NOTE_PIANO, 3, 3);
						player2.playSound(player1.getLocation(), Sound.NOTE_PIANO, 3, 3);
					}
				}
				if(time <= 0){
					if(state == SessionState.TELEPORTING){
						player1.teleport(map.getSpawn1());
						player2.teleport(map.getSpawn2());
						setTime(5);
						setState(SessionState.PREGAME);
						player1.getInventory().clear();
						player2.getInventory().clear();
						ItemStack is = ItemUtil.getItemStack("Load Default kit", Material.BOOK, ChatColor.GRAY + "Click to load kit");
						player1.getInventory().setItem(0, is);
						player2.getInventory().setItem(0, is);
						player1.getPlayer().setGameMode(GameMode.SURVIVAL);
						player2.getPlayer().setGameMode(GameMode.SURVIVAL);
						return;
					}
					if(state == SessionState.PREGAME){
						setTime(60);
						setState(SessionState.MATCHING);
						return;
					}
					if(state == SessionState.MATCHING){
						setTime(5);
						setState(SessionState.ENDING);
						return;
					}
					if(state == SessionState.ENDING){
						player1.setInMatch(false);
						player2.setInMatch(false);
						player1.setSession(null);
						player2.setSession(null);
						player1.teleport(plugin.getLobbyLocation());
						player2.teleport(plugin.getLobbyLocation());
						player1.getPlayer().getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
						player2.getPlayer().getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
						player1.clearArmor();
						player2.clearArmor();
						player1.setDefaultItems();
						player2.setDefaultItems();
						this.cancel();
						plugin.getSessionManager().getSessions().remove(plugin.getSessionManager().getSession(id));
						map.setUsing(false);
						return;
					}
				}
			}
		}.runTaskTimer(plugin, 0, 20);
	}

	public void setKitType(KitType kitType) {
		this.kitType = kitType;
	}
}