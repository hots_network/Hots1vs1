package tokyo.hots.game1vs1.data;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.util.ItemUtil;

public class LeaderBoardData {

	private List<PlayerData> data;

	public LeaderBoardData() {
		this.data = Lists.newArrayList();
	}

	public List<PlayerData> getData() {
		return data;
	}

	public void update(){
		data.clear();
		File file = new File(Hots1vs1Plugin.getPluginFolder() + "/playerdata/");
		String[] list = file.list();
		if (list != null){
			String[] arrayOfString1;
			int j = (arrayOfString1 = list).length;
			for (int i = 0; i < j; i++){
				String filename = arrayOfString1[i];
				PlayerData pd = new PlayerData(new File(Hots1vs1Plugin.getPluginFolder() + "/playerdata/" + filename));
				pd.load();
				data.add(pd);
			}
			Collections.sort(data, new Comparator<PlayerData>() {@Override public int compare(PlayerData o1, PlayerData o2) { return o1.getWon() > o2.getWon() ? -1 : 1;}});
		}
	}

	public int getRank(String name){
		for(int i = 0; i < data.size(); i++){
			if(data.get(i).getName() == name){
				i++;
				return i;
			}
		}
		return 0;
	}

	public PlayerData get(int i){
		return data.get(i);
	}

	public ItemStack getItemStack(int i){
		PlayerData data = null;
		data = getData().get(i);
		int rank = i+1;
		return ItemUtil.getSkullItem(data.getName(), "" + ChatColor.YELLOW + "#" + rank + " " + ChatColor.BLUE + data.getName(), "" + ChatColor.GOLD + data.getWon() + ChatColor.WHITE + " won", "" + ChatColor.GOLD + data.getLost() + ChatColor.WHITE + " lost");
	}
}