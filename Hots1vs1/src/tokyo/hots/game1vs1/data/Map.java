package tokyo.hots.game1vs1.data;

import org.bukkit.Location;

public class Map {

	private String name;

	private Location spawn1, spawn2;

	private boolean using;

	public Map(String name, Location spawn1, Location spawn2) {
		this.name = name;
		this.spawn1 = spawn1;
		this.spawn2 = spawn2;
		this.using = false;
	}

	public String getName() {
		return name;
	}

	public Location getSpawn1() {
		return spawn1;
	}

	public Location getSpawn2() {
		return spawn2;
	}

	public boolean isUsing() {
		return using;
	}

	public void setUsing(boolean using) {
		this.using = using;
	}
}
