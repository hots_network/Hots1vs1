package tokyo.hots.game1vs1.data;

import java.io.File;

import tokyo.hots.game1vs1.util.YamlEditor;

public class PlayerData {

	private YamlEditor edit;

	private int won, lost;

	private String name;

	public PlayerData(File file) {
		this.edit = new YamlEditor(file);
		this.won = 0;
		this.lost = 0;
		this.name = null;
	}

	public void load(){
		this.won = edit.getInt("Won");
		this.lost = edit.getInt("Lost");
		this.name = edit.getString("Name");
	}

	public int getWon() {
		return won;
	}

	public int getLost() {
		return lost;
	}

	public String getName() {
		return name;
	}

	public void setWon(int won) {
		this.won = won;
		edit.setValue("Won", won);
		edit.save();
	}

	public void setLost(int lost) {
		this.lost = lost;
		edit.setValue("Lost", lost);
		edit.save();
	}
}

