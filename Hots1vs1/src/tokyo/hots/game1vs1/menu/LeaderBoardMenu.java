package tokyo.hots.game1vs1.menu;

import tokyo.hots.game1vs1.Hots1vs1Plugin;

public class LeaderBoardMenu extends Menu{

	public LeaderBoardMenu(Hots1vs1Plugin plugin) {
		super("1vs1 Leader Board", 3, plugin);
	}

	@Override
	public void update() {
		for(int i = 0; i < getPlugin().getLeaderBoardData().getData().size(); i++){
			if(i <= getInventory().getSize()){
				setItem(i, getPlugin().getLeaderBoardData().getItemStack(i));
			}
		}
	}
}