package tokyo.hots.game1vs1.menu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.manager.SessionManager;
import tokyo.hots.game1vs1.session.Session;
import tokyo.hots.game1vs1.session.SessionState;
import tokyo.hots.game1vs1.util.ItemUtil;

public final class SessionMenu extends Menu{

	public SessionMenu(Hots1vs1Plugin plugin) {
		super("1vs1 Sessions", 6, plugin);
	}

	@Override
	public void update() {
		clear();
		ItemStack is = ItemUtil.getItemStack("" + ChatColor.GREEN + ChatColor.BOLD + "Create Session", Material.NAME_TAG, ChatColor.GRAY + "Click to create your session");
		ItemStack is2 = ItemUtil.getItemStack("" + ChatColor.RED + ChatColor.BOLD + "Leave Session", Material.FLINT_AND_STEEL, ChatColor.GRAY +  "Click to leave session");
		setItem(48, is);
		setItem(50, is2);
		SessionManager manager = getPlugin().getSessionManager();
		for(int i = 0; i < manager.getSessions().size(); i++){
			Session session = manager.getSessions().get(i);
			if(session.getState() == SessionState.WAITING && session.isEnabled()){
				setItem(i, ItemUtil.getItemStack("Session " + session.getId(), session.getMaterial(),
						" ",
						session.getStateText(),
						" ",
						ChatColor.BLUE + session.getPlayer1().getName(),
						" ", ChatColor.WHITE + "Kit: " + session.getKitTypeText()));
			}
			if(session.getState() != SessionState.WAITING){
				setItem(i, ItemUtil.getItemStack("Session " + session.getId(),
						session.getMaterial(),
						" ",
						session.getStateText(),
						" ",
						ChatColor.BLUE + session.getPlayer1().getName(),
						ChatColor.BLUE + session.getPlayer2().getName(),
						ChatColor.WHITE + "Kit: " + session.getKitTypeText()));
			}
		}
	}
}