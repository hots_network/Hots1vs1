package tokyo.hots.game1vs1.menu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.util.ItemUtil;

public class EditKitMenu extends Menu {

	public EditKitMenu(Hots1vs1Plugin plugin) {
		super("Edit Kit", 1, plugin);
	}

	@Override
	public void update() {
		ItemStack sg, pot, builduhc, gapplesg;
		sg = ItemUtil.getItemStack(ChatColor.GOLD + "SG", Material.FISHING_ROD, ChatColor.GRAY + "Click to edit your kit");
		pot = ItemUtil.getItemStack(ChatColor.GOLD + "Pot", Material.DIAMOND_CHESTPLATE, ChatColor.GRAY + "Click to edit your kit");
		builduhc = ItemUtil.getItemStack(ChatColor.GOLD + "BuildUHC", Material.WATER_BUCKET, ChatColor.GRAY + "Click to edit your kit");
		gapplesg = ItemUtil.getItemStack(ChatColor.GOLD + "GappleSG", Material.GOLDEN_APPLE, ChatColor.GRAY + "Click to edit your kit");
		setItem(1, sg);
		setItem(3, pot);
		setItem(5, builduhc);
		setItem(7, gapplesg);
	}
}
