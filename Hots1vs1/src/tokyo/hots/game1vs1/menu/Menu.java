package tokyo.hots.game1vs1.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import tokyo.hots.game1vs1.Hots1vs1Plugin;

public abstract class Menu {

	private String name;

	private Inventory inventory;

	private Hots1vs1Plugin plugin;

	public Menu(String name, int rows, Hots1vs1Plugin plugin) {
		this.name = name;
		this.plugin = plugin;
		this.inventory = Bukkit.createInventory(null, rows * 9, name);
		start();
	}

	public String getName() {
		return name;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void open(Player player){
		player.openInventory(this.inventory);
	}

	public abstract void update();

	public void start(){
		new BukkitRunnable() {
			@Override
			public void run() {
				update();
			}
		}.runTaskTimer(Hots1vs1Plugin.getPlugin(), 0, 10);
	}

	public void setItem(int slot, ItemStack is){
		inventory.setItem(slot, is);
	}

	public Hots1vs1Plugin getPlugin() {
		return plugin;
	}

	public void clear(){
		this.inventory.clear();
	}
}
