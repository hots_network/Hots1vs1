package tokyo.hots.game1vs1.task;

import org.bukkit.entity.Entity;

import tokyo.hots.game1vs1.Hots1vs1Plugin;

public class EntityRemovingDelayedTask extends Task{

	private Entity entity;

	public EntityRemovingDelayedTask(Hots1vs1Plugin plugin, Entity entity) {
		super(plugin);
		this.entity = entity;
		setDelay(20*5);
	}

	@Override
	public void tick() {
		entity.remove();
		this.cancel();
	}



}
