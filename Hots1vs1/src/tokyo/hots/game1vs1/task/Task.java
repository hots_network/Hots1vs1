package tokyo.hots.game1vs1.task;

import org.bukkit.scheduler.BukkitRunnable;

import tokyo.hots.game1vs1.Hots1vs1Plugin;

public abstract class Task extends BukkitRunnable{

	Hots1vs1Plugin plugin;

	int delay, period;

	public Task(Hots1vs1Plugin plugin) {
		this.plugin = plugin;
		this.delay = 0;
		this.period = 0;
	}

	public void runTaskTimer(){
		this.runTaskTimer(plugin, delay, period);
	}

	public void runTaskLater(){
		this.runTaskLater(plugin, delay);
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public abstract void tick();

	@Override
	public void run() {
		tick();
	}
}
