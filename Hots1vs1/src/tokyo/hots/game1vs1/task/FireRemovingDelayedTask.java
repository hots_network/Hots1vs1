package tokyo.hots.game1vs1.task;

import org.bukkit.Material;
import org.bukkit.block.Block;

import tokyo.hots.game1vs1.Hots1vs1Plugin;

public class FireRemovingDelayedTask extends Task{

	private Block block;

	public FireRemovingDelayedTask(Hots1vs1Plugin plugin, Block block) {
		super(plugin);
		this.block = block;
		setDelay(20*5);
	}

	@Override
	public void tick() {
		block.setType(Material.AIR);
		this.cancel();
	}

}
