package tokyo.hots.game1vs1.manager;

import java.util.List;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.session.Session;
import tokyo.hots.game1vs1.session.SessionPlayer;

public class SessionManager {

	private List<Session> sessions;

	Hots1vs1Plugin plugin;

	public SessionManager(Hots1vs1Plugin plugin) {
		this.sessions = Lists.newArrayList();
		this.plugin = plugin;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public Session newSession(SessionPlayer player){
		return new Session(player, plugin);
	}

	public void addSession(Session session){
		sessions.add(session);
	}

	public void create(SessionPlayer player){
		if(player.isQueue()){
			player.sendMessage("You have already queued");
			return;
		}
		Session session = newSession(player);
		addSession(session);
		player.setQueue(true);
		player.setSession(session);
	}

	public Session getSession(int id){
		for(Session session : sessions){
			if(session.getId() == id){
				return session;
			}
		}
		return null;
	}

	public int getInGame(){
		int i = 0;
		for(SessionPlayer player : plugin.getSessionPlayerManager().getSessionPlayers()){
			if(player.isInMatch()){
				i++;
			}
		}
		return i;
	}

	public int getInLobby(){
		int i = plugin.getSessionPlayerManager().getSessionPlayers().size() - getInGame();
		return i;
	}
}