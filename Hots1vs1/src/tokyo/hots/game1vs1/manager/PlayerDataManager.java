package tokyo.hots.game1vs1.manager;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.util.ChatMessage;
import tokyo.hots.game1vs1.util.YamlEditor;

public class PlayerDataManager {

	Hots1vs1Plugin plugin;

	private final String DIR = Hots1vs1Plugin.getPluginFolder() + "/playerdata/";

	public PlayerDataManager(Hots1vs1Plugin plugin) {
		this.plugin = plugin;
	}

	public boolean isExists(String uuid){
		File file = new File(DIR + uuid + ".yml");
		if(file.exists()){
			return true;
		}
		return false;
	}

	public String toNameFromUUID(String uuid){
		File file = new File(DIR);
		String[] list = file.list();
		if (list != null){
			String[] arrayOfString1;
			int j = (arrayOfString1 = list).length;
			for (int i = 0; i < j; i++){
				String filename = arrayOfString1[i];
				filename = filename.substring(0, filename.indexOf(".yml"));
				if(filename == uuid){
					YamlEditor edit = new YamlEditor(new File(DIR + uuid + ".yml"));
					return edit.getString("Name");
				}
			}
		}
		return null;
	}

	public void create(Player player){
		ChatMessage.send(player, "Creating your player data...");
		YamlEditor edit = new YamlEditor(new File(DIR + player.getUniqueId().toString() + ".yml"));
		edit.setValue("Name", player.getName());
		edit.setValue("Won", 0);
		edit.setValue("Lost", 0);
		edit.setValue("SG_Kit0", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit1", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit2", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit3", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit4", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit5", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit6", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("SG_Kit7", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit0", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit1", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit2", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit3", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit4", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit5", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit6", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("GappleSG_Kit7", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit0", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit1", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit2", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit3", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit4", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit5", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit6", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit7", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("Pot_Kit0", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit1", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit2", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit3", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit4", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit5", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit6", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.setValue("BuildUHC_Kit7", Lists.newArrayList(new ArrayList<ItemStack>()));
		edit.save();
		ChatMessage.send(player, "Successfully. Created your player data.");
	}

	public String toPlayerDataPath(String uuid){
		return DIR + uuid + ".yml";
	}
}
