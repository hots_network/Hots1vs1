package tokyo.hots.game1vs1.manager;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.data.Map;
import tokyo.hots.game1vs1.util.ChatMessage;
import tokyo.hots.game1vs1.util.YamlEditor;

public class MapManager {

	private List<Map> maps;

	private String dir;

	public MapManager() {
		this.maps = Lists.newArrayList();
		this.dir = Hots1vs1Plugin.getPluginFolder() + "/maps/";
	}

	public List<Map> getMaps() {
		return maps;
	}

	public void load(){
		File dir2 = new File(dir);
		String[] list = dir2.list();
		if (list != null){
			String[] arrayOfString1;
			int j = (arrayOfString1 = list).length;
			for (int i = 0; i < j; i++){
				String filename = arrayOfString1[i];
				YamlEditor edit = new YamlEditor(new File(this.dir + filename));
				filename = filename.substring(0, filename.indexOf(".yml"));
				register(new Map(filename, edit.getLocation("Spawn1.World", "Spawn1.X", "Spawn1.Y", "Spawn1.Z", "Spawn1.Yaw", "Spawn1.Pitch"),
						edit.getLocation("Spawn2.World", "Spawn2.X", "Spawn2.Y", "Spawn2.Z", "Spawn2.Yaw", "Spawn2.Pitch")));
			}
		}
	}

	public Map getMap(String name){
		for(Map map : maps){
			if(map.getName() == name){
				return map;
			}
		}
		return null;
	}

	public Map newMap(String name, Location spawn1, Location spawn2){
		return new Map(name, spawn1, spawn2);
	}

	public void register(Map map){
		maps.add(map);
	}

	public Map randomMap(){
		List<Map> list = Lists.newArrayList();
		for(Map map : maps){
			if(!map.isUsing()){
				list.add(map);
				if(list.isEmpty() || list == null)return null;
				Collections.shuffle(list);
				return list.get(0);
			}
		}
		return null;
	}

	public void create(CommandSender sender, String name){
		File file = new File(dir + name + ".yml");
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ChatMessage.send(sender, "Successfully. New map has created.");
	}

	public void setSpawn(Player player, int num, String mapName){
		//if(num != 1 || num != 2){ChatMessage.send(player, "1 or 2");return;}
		YamlEditor edit = new YamlEditor(new File(dir + mapName + ".yml"));
		Location loc = player.getLocation();
		edit.setValue("Spawn" + num + ".World", player.getWorld().getName());
		edit.setValue("Spawn" + num + ".X", loc.getX());
		edit.setValue("Spawn" + num + ".Y", loc.getY());
		edit.setValue("Spawn" + num + ".Z", loc.getZ());
		edit.setValue("Spawn" + num + ".Yaw", loc.getYaw());
		edit.setValue("Spawn" + num + ".Pitch", loc.getPitch());
		edit.save();
		ChatMessage.send(player, "Successfully. " + mapName + "'s spawn" + num + " has saved");
	}
}
