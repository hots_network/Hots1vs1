package tokyo.hots.game1vs1.manager;

import java.io.File;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.session.kit.Kit;
import tokyo.hots.game1vs1.session.kit.KitType;
import tokyo.hots.game1vs1.util.ChatMessage;
import tokyo.hots.game1vs1.util.YamlEditor;

public class KitManager {

	public final static String SG_PATH = Hots1vs1Plugin.getPlugin().getDataFolder() + "/kit/SG.yml";

	public final static String GAPPLESG_PATH = Hots1vs1Plugin.getPlugin().getDataFolder() + "/kit/GappleSG.yml";

	public final static String POT_PATH = Hots1vs1Plugin.getPlugin().getDataFolder() + "/kit/Pot.yml";

	public final static String BUILD_UHC_PATH = Hots1vs1Plugin.getPlugin().getDataFolder() + "/kit/BuildUHC.yml";

	Hots1vs1Plugin plugin;

	private Kit SG_Kit, GappleSG_Kit, Pot_Kit, BuildUHC_Kit;

	public KitManager(Hots1vs1Plugin plugin) {
		this.plugin = plugin;
		this.SG_Kit = new Kit(KitType.SG, new YamlEditor(new File(SG_PATH)));
		this.GappleSG_Kit = new Kit(KitType.GAPPLE_SG, new YamlEditor(new File(GAPPLESG_PATH)));
		this.Pot_Kit = new Kit(KitType.POT, new YamlEditor(new File(POT_PATH)));
		this.BuildUHC_Kit = new Kit(KitType.BUILD_UHC, new YamlEditor(new File(BUILD_UHC_PATH)));
	}

	public Kit getSG_Kit() {
		return SG_Kit;
	}

	public Kit getGappleSG_Kit() {
		return GappleSG_Kit;
	}

	public Kit getPot_Kit() {
		return Pot_Kit;
	}

	public Kit getBuildUHC_Kit() {
		return BuildUHC_Kit;
	}

	public void save(Player player, String kitType){
		YamlEditor edit = null;
		List<ItemStack> items, armors;
		items = Lists.newArrayList();
		armors = Lists.newArrayList();
		Inventory inv = player.getInventory();
		for(int i = 0; i < inv.getSize(); i++){
			items.add(inv.getItem(i));
		}
		ItemStack[] armor = player.getInventory().getArmorContents();
		for(int i = 0; i < armor.length; i++){
			armors.add(armor[i]);
		}
		switch (kitType) {
		case "sg":
			edit = new YamlEditor(new File(SG_PATH));
			edit.setValue("Items", items);
			edit.setValue("Armors", armors);
			break;
		case "gapplesg":
			edit = new YamlEditor(new File(GAPPLESG_PATH));
			edit.setValue("Items", items);
			edit.setValue("Armors", armors);
			break;
		case "pot":
			edit = new YamlEditor(new File(POT_PATH));
			edit.setValue("Items", items);
			edit.setValue("Armors", armors);
			break;
		case "builduhc":
			edit = new YamlEditor(new File(BUILD_UHC_PATH));
			edit.setValue("Items", items);
			edit.setValue("Armors", armors);
			break;
		default:
			ChatMessage.send(player, "Invaild kit name");
			break;
		}
		edit.save();
		ChatMessage.send(player, "Successfully. Saved kit");
	}
}
