package tokyo.hots.game1vs1.manager;

import java.util.List;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.session.SessionPlayer;

public class SessionPlayerManager {

	private List<SessionPlayer> sessionPlayers;

	Hots1vs1Plugin plugin;

	public SessionPlayerManager(Hots1vs1Plugin plugin) {
		sessionPlayers = Lists.newArrayList();
		this.plugin = plugin;
	}

	public List<SessionPlayer> getSessionPlayers() {
		return sessionPlayers;
	}

	public SessionPlayer getSessionPlayer(Player player){
		for(SessionPlayer players : sessionPlayers){
			if(players.getPlayer() == player){
				return players;
			}
		}
		return null;
	}

	public SessionPlayer newSessionPlayer(Player player){
		return new SessionPlayer(player, plugin);
	}

	public void register(SessionPlayer sessionPlayer){
		sessionPlayers.add(sessionPlayer);
	}

	public void unregister(SessionPlayer sessionPlayer){
		sessionPlayers.remove(sessionPlayer);
	}
}
