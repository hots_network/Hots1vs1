package tokyo.hots.game1vs1.manager;

import org.bukkit.entity.Player;

import tokyo.hots.game1vs1.Hots1vs1Plugin;
import tokyo.hots.game1vs1.menu.EditKitMenu;
import tokyo.hots.game1vs1.menu.KitMenu;
import tokyo.hots.game1vs1.menu.LeaderBoardMenu;
import tokyo.hots.game1vs1.menu.Menu;
import tokyo.hots.game1vs1.menu.SessionMenu;

public class MenuManager {

	private KitMenu kitMenu;

	private SessionMenu sessionMenu;

	private EditKitMenu editKitMenu;

	private LeaderBoardMenu leaderBoardMenu;

	Hots1vs1Plugin plugin;

	public MenuManager(Hots1vs1Plugin plugin) {
		this.kitMenu = new KitMenu(plugin);
		this.sessionMenu = new SessionMenu(plugin);
		this.editKitMenu = new EditKitMenu(plugin);
		this.leaderBoardMenu = new LeaderBoardMenu(plugin);
		this.plugin = plugin;
	}

	public KitMenu getKitMenu() {
		return kitMenu;
	}

	public SessionMenu getSessionMenu() {
		return sessionMenu;
	}

	public EditKitMenu getEditKitMenu() {
		return editKitMenu;
	}

	public LeaderBoardMenu getLeaderBoardMenu() {
		return leaderBoardMenu;
	}

	public Menu createSettingMenu(Player player){
		return new Menu(player.getName() + " Settings", 2, plugin) {
			@Override
			public void update() {

			}
		};
	}

	public Menu createProfileMenu(Player player){
		return new Menu(player.getName() + " Profile", 1, plugin) {
			@Override
			public void update() {

			}
		};
	}
}
