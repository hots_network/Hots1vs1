package tokyo.hots.game1vs1.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class YamlEditor {

	private File file;

	private YamlConfiguration yml;

	public YamlEditor(File file) {
		this.file = file;
		if(file != null){
			this.yml = YamlConfiguration.loadConfiguration(file);
		}
	}

	public void setValue(String value, Object object){
		yml.set(value, object);
	}

	public double getDouble(String key) {
		return this.yml.getDouble(key);
	}

	public int getInt(String key){
		return this.yml.getInt(key);
	}

	public String getString(String value){
		return yml.getString(value);
	}

	public Location getLocation(String world, String x, String y, String z, String yaw, String pitch){
		World w = Bukkit.getWorld(getString(world));
		double x1 = getDouble(x);
		double y1 = getDouble(y);
		double z1 = getDouble(z);
		float yaw1 = (float)getDouble(yaw);
		float pitch1 = (float)getDouble(pitch);
		return new Location(w , x1, y1, z1, yaw1, pitch1);
	}

	@SuppressWarnings("unchecked")
	public List<ItemStack> getItemStackList(String key){
		return (List<ItemStack>) this.yml.getList(key);
	}

	public void save(){
		try {
			yml.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
