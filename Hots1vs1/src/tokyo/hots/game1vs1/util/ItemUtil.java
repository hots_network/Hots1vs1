package tokyo.hots.game1vs1.util;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemUtil {

	public static ItemStack getItemStack(String name, Material mat, String... lore){
		ItemStack is = new ItemStack(mat);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null) {
			meta.setLore(Arrays.asList(lore));
		}
		is.setItemMeta(meta);
		return is;
	}

	public static ItemStack getSkullItem(String name, String displayName, String... lore){
		ItemStack is = new ItemStack(Material.SKULL_ITEM);
		SkullMeta meta = (SkullMeta) is.getItemMeta();
		meta.setOwner(name);
		meta.setDisplayName(displayName);
		if(lore != null){
			meta.setLore(Arrays.asList(lore));
		}
		is.setItemMeta(meta);
		return is;
	}
}
