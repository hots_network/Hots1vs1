package tokyo.hots.game1vs1;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import tokyo.hots.game1vs1.data.PlayerData;
import tokyo.hots.game1vs1.manager.MenuManager;
import tokyo.hots.game1vs1.manager.PlayerDataManager;
import tokyo.hots.game1vs1.manager.SessionPlayerManager;
import tokyo.hots.game1vs1.menu.SessionMenu;
import tokyo.hots.game1vs1.session.Session;
import tokyo.hots.game1vs1.session.SessionPlayer;
import tokyo.hots.game1vs1.session.SessionState;
import tokyo.hots.game1vs1.session.kit.KitType;
import tokyo.hots.game1vs1.task.EntityRemovingDelayedTask;
import tokyo.hots.game1vs1.task.FireRemovingDelayedTask;

public class PlayerListener implements Listener {

	Hots1vs1Plugin plugin;

	public PlayerListener(Hots1vs1Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		SessionPlayerManager manager = plugin.getSessionPlayerManager();
		manager.register(manager.newSessionPlayer(player)); //プレイヤー登録
		manager.getSessionPlayer(player).setDefaultItems();
		player.setGameMode(GameMode.SURVIVAL);
		manager.getSessionPlayer(player).clearArmor();
		player.setHealth(20.0);
		player.setFoodLevel(20);
		player.teleport(plugin.getLobbyLocation());
		PlayerDataManager pdm = plugin.getPlayerDataManager();
		SessionPlayer splayer = manager.getSessionPlayer(player);
		String uuid = player.getUniqueId().toString();
		if(!pdm.isExists(uuid)){
			pdm.create(player);
			splayer.setPlayerData(new PlayerData(new File(pdm.toPlayerDataPath(uuid))));
			return;
		}
		splayer.sendMessage("Loading your data...");
		PlayerData data = new PlayerData(new File(pdm.toPlayerDataPath(uuid)));
		data.load();
		splayer.setPlayerData(data);
		splayer.sendMessage("Complete. Loaded your data.");
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player player = e.getPlayer();
		SessionPlayerManager manager = plugin.getSessionPlayerManager();
		SessionPlayer sessionPlayer = manager.getSessionPlayer(player);
		if(sessionPlayer == null)return;
		if(sessionPlayer.isQueue()){
			sessionPlayer.leaveSession();
		}
		manager.unregister(sessionPlayer);
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if(!(e.getEntity() instanceof Player))return;
		Player player = (Player) e.getEntity();
		SessionPlayer splayer = plugin.getSessionPlayerManager().getSessionPlayer(player);
		e.getDrops().clear();
		if(splayer.isInMatch()){
			Session session = splayer.getSession();
			session.setTime(5);
			session.setState(SessionState.ENDING);
			Player killer = player.getKiller();
			splayer.sendMessage(ChatColor.BLUE + killer.getName() + "'s " + ChatColor.WHITE + "Health: " + ChatColor.RED + plugin.getSessionPlayerManager().getSessionPlayer(killer).getHealth());
		}
	}


	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player player = (Player) e.getWhoClicked();
		SessionPlayer splayer = plugin.getSessionPlayerManager().getSessionPlayer(player);
		Inventory inv = e.getInventory();
		ItemStack clicked = e.getCurrentItem();
		MenuManager manager = plugin.getMenuManager();
		if(clicked == null || splayer == null|| player == null || inv == null)return;
		if(inv.getName() == "1vs1 Sessions"){
			if(clicked.getType() == Material.NAME_TAG){
				if(splayer.isQueue()){
					splayer.sendMessage("You have already queued");
					e.setCancelled(true);
					player.closeInventory();
					return;
				}
				plugin.getSessionManager().create(splayer);
				e.setCancelled(true);
				player.closeInventory();
				splayer.openMenu(manager.getKitMenu());
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
			if(clicked.getType() == Material.FLINT_AND_STEEL){
				splayer.leaveSession();
				e.setCancelled(true);
				player.closeInventory();
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
			if(clicked.getType() == Material.EMERALD_BLOCK){
				if(splayer.isQueue()){
					splayer.sendMessage("You have already queued");
					e.setCancelled(true);
					player.closeInventory();
					splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
					return;
				}
				if(splayer.isInMatch()){
					splayer.sendMessage("You have already been in match");
					e.setCancelled(true);
					player.closeInventory();
					splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
					return;
				}
				String name = clicked.getItemMeta().getDisplayName();
				String array[] = name.split(" ");
				int id = new Integer(array[1]);
				Session session = plugin.getSessionManager().getSession(id);
				splayer.setSession(session);
				session.setPlayer2(splayer);
				session.start();
				e.setCancelled(true);
				player.closeInventory();
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
			if(clicked.getType() == Material.REDSTONE_BLOCK){
				e.setCancelled(true);
				player.closeInventory();
				splayer.sendMessage("This session is matching now");
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
		}
		SessionMenu sessionMenu = manager.getSessionMenu();
		if(inv.getName() == "Choose Kit"){
			if(clicked.getType() == Material.FISHING_ROD){
				splayer.getSession().setKitType(KitType.SG);
				splayer.getSession().setEnabled(true);
				e.setCancelled(true);
				player.closeInventory();
				splayer.openMenu(sessionMenu);
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
			if(clicked.getType() == Material.DIAMOND_CHESTPLATE){
				splayer.getSession().setKitType(KitType.POT);
				splayer.getSession().setEnabled(true);
				e.setCancelled(true);
				player.closeInventory();
				splayer.openMenu(sessionMenu);
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
			if(clicked.getType() == Material.WATER_BUCKET){
				splayer.getSession().setKitType(KitType.BUILD_UHC);
				splayer.getSession().setEnabled(true);
				e.setCancelled(true);
				player.closeInventory();
				splayer.openMenu(sessionMenu);
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
			if(clicked.getType() == Material.GOLDEN_APPLE){
				splayer.getSession().setKitType(KitType.GAPPLE_SG);
				splayer.getSession().setEnabled(true);
				e.setCancelled(true);
				player.closeInventory();
				splayer.openMenu(sessionMenu);
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				return;
			}
		}
		if(inv.getName() == "Edit Kit"){
			if(clicked.getType() == Material.FISHING_ROD){
				e.setCancelled(true);
				player.closeInventory();
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				splayer.teleport(plugin.getLocations().getSg_editkit());
				return;
			}
			if(clicked.getType() == Material.DIAMOND_CHESTPLATE){
				e.setCancelled(true);
				player.closeInventory();
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				splayer.teleport(plugin.getLocations().getPot_editkit());
				return;
			}
			if(clicked.getType() == Material.WATER_BUCKET){
				e.setCancelled(true);
				player.closeInventory();
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				splayer.teleport(plugin.getLocations().getBuilduhc_editkit());
				return;
			}
			if(clicked.getType() == Material.GOLDEN_APPLE){
				e.setCancelled(true);
				player.closeInventory();
				splayer.playSound(splayer.getLocation(), Sound.NOTE_PIANO, 1, 1);
				splayer.teleport(plugin.getLocations().getGapplesg_editkit());
				return;
			}
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player player = e.getPlayer();
		SessionPlayer sPlayer = plugin.getSessionPlayerManager().getSessionPlayer(player);
		ItemStack is = player.getItemInHand();
		Material type = is.getType();
		if(type == null || is == null || player == null || sPlayer == null)return;
		if(is.hasItemMeta()){
			String name = is.getItemMeta().getDisplayName();
			MenuManager manager = plugin.getMenuManager();
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR){
				if(is.getItemMeta().hasDisplayName()){
					if(type == Material.DIAMOND_SWORD){
						if(name.contains("Match Sessions")){
							sPlayer.openMenu(manager.getSessionMenu());
							return;
						}
					}
					if(type == Material.REDSTONE_COMPARATOR){
						if(name.contains("Settings")){
							sPlayer.openMenu(manager.createSettingMenu(player));
							return;
						}
					}
					if(type == Material.WATCH){
						if(name.contains("My Profile")){
							sPlayer.openMenu(manager.createProfileMenu(player));
							return;
						}
					}
					if(type == Material.ANVIL){
						if(name.contains("Edit Kit")){
							sPlayer.openMenu(manager.getEditKitMenu());
							return;
						}
					}
					if(type == Material.BOOK){
						if(name.contains("Leader Board")){
							sPlayer.openMenu(manager.getLeaderBoardMenu());
							return;
						}
						if(name.contains("Default")){
							sPlayer.setKit(sPlayer.getSession().getKit());
						}
					}
				}
			}
		}
	}


	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		Player player = e.getPlayer();
		SessionPlayer sp = plugin.getSessionPlayerManager().getSessionPlayer(player);
		if(sp == null)return;
		if(player.getGameMode() != GameMode.CREATIVE){
			if(sp.getSession() == null){
				e.setCancelled(true);
				return;
			}
			if(sp.getSession().getKitType() != KitType.SG && !sp.isInMatch() || sp.getSession().getKitType() != KitType.BUILD_UHC && !sp.isInMatch()){
				e.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e){
		Player player = e.getPlayer();
		SessionPlayer sp = plugin.getSessionPlayerManager().getSessionPlayer(player);
		if(sp == null)return;
		if(player.getGameMode() != GameMode.CREATIVE){
			if(sp.getSession() == null){
				e.setCancelled(true);
				return;
			}
			if(sp.getSession().getKitType() != KitType.BUILD_UHC && !sp.isInMatch()){
				e.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onChange(FoodLevelChangeEvent e){
		if(!(e.getEntity() instanceof Player))return;
		SessionPlayer player = plugin.getSessionPlayerManager().getSessionPlayer((Player)e.getEntity());
		if(!player.isInMatch()){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		SessionPlayer sp = plugin.getSessionPlayerManager().getSessionPlayer(e.getPlayer());
		if(!sp.isInMatch()){
			e.setCancelled(true);
			return;
		}
		EntityRemovingDelayedTask task = new EntityRemovingDelayedTask(plugin, e.getItemDrop());
		task.runTaskLater();
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e){
		if(!(e.getEntity() instanceof Player))return;
		Player player = (Player) e.getEntity();
		SessionPlayer sp = plugin.getSessionPlayerManager().getSessionPlayer(player);
		if(!sp.isInMatch()){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onIgnite(BlockIgniteEvent e){
		if(e.getCause() ==  IgniteCause.FLINT_AND_STEEL){
			FireRemovingDelayedTask task = new FireRemovingDelayedTask(plugin, e.getBlock());
			task.runTaskLater();
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		e.setRespawnLocation(plugin.getLobbyLocation());
	}
}