package tokyo.hots.game1vs1;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import tokyo.hots.game1vs1.data.LeaderBoardData;
import tokyo.hots.game1vs1.manager.KitManager;
import tokyo.hots.game1vs1.manager.MapManager;
import tokyo.hots.game1vs1.manager.MenuManager;
import tokyo.hots.game1vs1.manager.PlayerDataManager;
import tokyo.hots.game1vs1.manager.SessionManager;
import tokyo.hots.game1vs1.manager.SessionPlayerManager;
import tokyo.hots.game1vs1.task.Task;
import tokyo.hots.game1vs1.util.YamlEditor;

public class Hots1vs1Plugin extends JavaPlugin {

	//インスタンス
	private static Hots1vs1Plugin plugin;

	//セッション管理
	private SessionManager sessionManager;

	//セッションプレイヤー管理
	private SessionPlayerManager sessionPlayerManager;

	//マップ管理
	private MapManager mapManager;

	//メニュー管理
	private MenuManager menuManager;

	//キット管理
	private KitManager kitManager;

	//プレイヤーデータ管理
	private PlayerDataManager playerDataManager;

	//ロビー座標
	private Location lobbyLocation;

	//サイドバーオブジェクト
	private Sidebar sidebar;

	//リーダーボードデータ
	private LeaderBoardData leaderBoardData;

	//座標たち
	private Locations locations;

	public void onEnable(){
		plugin = this;
		this.locations = new Locations(this);
		this.sessionManager = new SessionManager(this);
		this.sessionPlayerManager = new SessionPlayerManager(this);
		this.mapManager = new MapManager();
		this.menuManager = new MenuManager(this);
		this.kitManager = new KitManager(this);
		this.playerDataManager = new PlayerDataManager(this);
		this.leaderBoardData = new LeaderBoardData();
		this.getCommand("1vs1").setExecutor(new Commands(this));
		Bukkit.getPluginManager().removePermission(new Permission("1vs1.use.cmd"));
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		mapManager.load();
		this.lobbyLocation = new YamlEditor(new File(getPluginFolder() + "/lobby.yml")).getLocation("Lobby.World", "Lobby.X", "Lobby.Y", "Lobby.Z", "Lobby.Yaw", "Lobby.Pitch");
		this.sidebar = new Sidebar(this);
		runTask();
	}

	public Locations getLocations() {
		return locations;
	}

	private void runTask(){
		Task sideBarTask = new Task(this) {
			@Override
			public void tick() {
				sidebar.update();
			}
		};
		sideBarTask.setDelay(0);
		sideBarTask.setPeriod(20);
		sideBarTask.runTaskTimer();

		Task leaderboardTask = new Task(this) {
			@Override
			public void tick() {
				leaderBoardData.update();
			}
		};
		leaderboardTask.setDelay(0);
		leaderboardTask.setPeriod(20*5);
		leaderboardTask.runTaskTimer();
	}

	public LeaderBoardData getLeaderBoardData() {
		return leaderBoardData;
	}

	public Sidebar getSidebar() {
		return sidebar;
	}

	public static Hots1vs1Plugin getPlugin() {
		return plugin;
	}

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public SessionPlayerManager getSessionPlayerManager() {
		return sessionPlayerManager;
	}

	public MapManager getMapManager() {
		return mapManager;
	}

	public MenuManager getMenuManager() {
		return menuManager;
	}

	public KitManager getKitManager() {
		return kitManager;
	}

	public PlayerDataManager getPlayerDataManager() {
		return playerDataManager;
	}

	public static String getPluginFolder(){
		return plugin.getDataFolder().getAbsolutePath();
	}

	public Location getLobbyLocation() {
		return lobbyLocation;
	}

	public void setLobby(Player player){
		YamlEditor edit = new YamlEditor(new File(getPluginFolder() + "/lobby.yml"));
		Location loc = player.getLocation();
		edit.setValue("Lobby.World", player.getWorld().getName());
		edit.setValue("Lobby.X", loc.getX());
		edit.setValue("Lobby.Y", loc.getY());
		edit.setValue("Lobby.Z", loc.getZ());
		edit.setValue("Lobby.Yaw", loc.getYaw());
		edit.setValue("Lobby.Pitch", loc.getPitch());
		edit.save();
		getSessionPlayerManager().getSessionPlayer(player).sendMessage("Successfully. Lobby location has saved.");
	}
}
